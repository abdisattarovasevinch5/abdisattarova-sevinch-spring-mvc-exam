<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 02/25/2022
  Time: 9:51 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <title>Books</title>
</head>
<body>
<c:choose>
    <c:when test="${user.role.equals('ADMIN')}">
        <div class="row">
            <a href='/books/form' class="btn btn-primary" style=
                    "margin-left: 16px; float: left; margin-bottom: 2rem"
               onMouseOver="this.style.color='#0F0'"
               onMouseOut="this.style.color='#00F'">
                <i class="fas fa-plus"></i> Add book</a>
        </div>
    </c:when>
</c:choose>
<div class="col-md-12">
    <table class="table table-bordered">
        <thead>
        <tr style="text-align: center">
            <th scope="col">Book</th>
            <%--            <th>Authors</th>--%>
            <c:choose>
                <c:when test="${user.role.equals('ADMIN')}">
                    <th scope="col" colspan="2">Settings</th>
                </c:when>
            </c:choose>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${books}" var="book" varStatus="loop">
            <tr scope="row" style="text-align: center">
                <td>
                   <%-- <a href="/books/info/${book.id}">--%>
                        <h5 class="card-title">${book.name}</h5>
                  <%--  </a>--%>
                </td>
                    <%--     <c:forEach items="${book.authors}" var="author">
                             <td>
                                 <a href="/users/info/${author.id}"> ${author.fullName}</a><br>
                             </td>
                         </c:forEach>--%>
                <c:choose>
                    <c:when test="${user.role.equals('ADMIN')}">
                        <td><a class="btn btn-info" href='/books/form?id=${book.id}'><i
                                class="fas fa-edit"></i>
                        </a></td>
                        <td><a class="btn btn-danger" href="/books/delete/${book.id}"><i
                                class="fas fa-trash"></i> </a></td>
                    </c:when>
                </c:choose>
            </tr>
        </c:forEach>
        </tbody>
        <tbody></tbody>
        <tbody></tbody>
    </table>
</div>
</body>
</html>
