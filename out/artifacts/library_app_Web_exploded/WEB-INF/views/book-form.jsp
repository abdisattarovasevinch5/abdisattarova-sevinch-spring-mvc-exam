<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 02/25/2022
  Time: 11:40 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Book form</title>

</head>
<body>
<div class="row">
    <div class="col-md-6 offset-3">
        <form action="/books" method="post">

            <c:if test="${book.id !=null}">
                <div class="form-group">
                    <input hidden name="id" type="text" class="form-control" id="taskId"
                           value="${book.id}">
                </div>
            </c:if>

            <div class="form-group">
                <label for="taskTitle">Name: </label>
                <input
                <c:if test="${book.id !=null}">
                        value="${book.name}"
                </c:if>

                        name="name" type="text" class="form-control" id="taskTitle"
                        placeholder="Input book name here">
            </div>
            <button type="submit" class="btn btn-success">Save</button>
        </form>
    </div>
</div>
</body>
</html>
