<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 02/25/2022
  Time: 8:56 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
          rel="stylesheet"
          integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
          crossorigin="anonymous">
</head>
<body>
<div style="justify-content: center">
    <div style="margin-left: 500px ; margin-top: 50px">
        <a href="/login/form" class="btn btn-primary"
           style="padding: 20px 60px"
           onMouseOver="this.style.color='#0F0'"
           onMouseOut="this.style.color='#00F'">
            <ion-icon name="people-outline"></ion-icon>
            Login</a>
        <a href="/register/form" class="btn btn-primary mx-100"
           style="padding: 20px 60px"
           onMouseOver="this.style.color='#0F0'"
           onMouseOut="this.style.color='#00F'">
            <ion-icon name="book-outline"></ion-icon>
            Register</a>
    </div>
</div>
</body>
</html>
