package uz.sevinch.model;
//Sevinch Abdisattorova 02/25/2022 8:46 AM

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.PackagePrivate;

import javax.persistence.*;

@AllArgsConstructor
@NoArgsConstructor
@Data
@PackagePrivate
@Entity(name = "users")
public class User {
    @Id
    @GeneratedValue
    Integer id;

    String fullName;

    String role;

    @Column(unique = true)
    String username;

    String password;

}
