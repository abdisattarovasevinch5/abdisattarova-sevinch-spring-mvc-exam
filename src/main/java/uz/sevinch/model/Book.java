package uz.sevinch.model;
//Sevinch Abdisattorova 02/25/2022 9:07 AM

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.PackagePrivate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity(name = "books")
@PackagePrivate
public class Book {
    @Id
    @GeneratedValue
    Integer id;
    private String name;

//    @ManyToMany
//    private List<User> authors;
}
