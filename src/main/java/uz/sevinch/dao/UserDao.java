package uz.sevinch.dao;
//Sevinch Abdisattorova 02/25/2022 9:32 AM

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Example;
import org.hibernate.query.NativeQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import uz.sevinch.model.User;

@Repository
public class UserDao {

    @Autowired
    SessionFactory sessionFactory;

    @Autowired
    JdbcTemplate template;


    public User getUserFromDb(User user) {
        User userRes = null;
        try {
            String queryStr = "select * from users where username='" +
                    user.getUsername() + "' and password ='" + user.getPassword() + "';";
            userRes = template.queryForObject(queryStr, (rs, row) -> {
                User user1 = new User();
                user1.setId(rs.getInt(1));
                user1.setFullName(rs.getString(2));
                user1.setPassword(rs.getString(3));
                user1.setRole(rs.getString(4));
                user1.setUsername(rs.getString(5));
                return user1;
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
        return userRes;
    }

    public boolean saveUser(User user) {
        user.setRole("USER");
        Session currentSession = sessionFactory.getCurrentSession();
        try {
            currentSession.saveOrUpdate(user);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
}
