package uz.sevinch.dao;
//Sevinch Abdisattorova 02/25/2022 10:30 AM

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.NativeQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import uz.sevinch.model.Book;

import java.util.List;

@Repository
public class BookDao {

    @Autowired
    SessionFactory sessionFactory;

    public List<Book> getAllBooks() {
        Session session = sessionFactory.getCurrentSession();
        NativeQuery nativeQuery = session.createNativeQuery("select b.id,\n" +
                "       b.name " +
                "from books b");
        nativeQuery.addEntity(Book.class);
        List<Book> books = nativeQuery.list();
        return books;

    }

    public Book getBookById(int id) {
        Session currentSession = sessionFactory.getCurrentSession();
        Book book = currentSession.get(Book.class, id);
        return book;
    }

    public void saveBook(Book book) {
        Session session = sessionFactory.getCurrentSession();
        session.saveOrUpdate(book);
    }

    public void deleteBookById(Integer id) {
        Session currentSession = sessionFactory.getCurrentSession();
        Book bookById = getBookById(id);
        currentSession.delete(bookById);
    }
}
