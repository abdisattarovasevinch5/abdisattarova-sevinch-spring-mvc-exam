package uz.sevinch.service;
//Sevinch Abdisattorova 02/25/2022 10:03 AM

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import uz.sevinch.dao.BookDao;
import uz.sevinch.model.Book;

import javax.servlet.http.HttpSession;
import javax.transaction.Transactional;
import java.util.List;

@Service
public class BookService {

    @Autowired
    BookDao bookDao;

    @Transactional
    public void getAllBooks(Model model, HttpSession httpSession) {
        List<Book> bookList = bookDao.getAllBooks();
        model.addAttribute("books", bookList);
        model.addAttribute("user", httpSession.getAttribute("user"));
    }

    @Transactional
    public Book getBookById(int id) {
        return bookDao.getBookById(id);
    }

    @Transactional
    public void saveBook(Book book) {
        bookDao.saveBook(book);
    }

    @Transactional
    public void deleteBookById(Integer id) {
        bookDao.deleteBookById(id);
    }
}
