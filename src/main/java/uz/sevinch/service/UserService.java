package uz.sevinch.service;
//Sevinch Abdisattorova 02/25/2022 9:24 AM

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import uz.sevinch.dao.UserDao;
import uz.sevinch.model.User;

import javax.servlet.http.HttpSession;
import javax.transaction.Transactional;

@Service
public class UserService {

    @Autowired
    UserDao userDao;

    @Transactional
    public boolean getUserByUsernameAndPassword(User user, Model model, HttpSession httpSession) {
        User userFromDb = userDao.getUserFromDb(user);
       if (userFromDb == null) {
            String msg = "Username or password is incorrect!";
            model.addAttribute("msg", msg);
            return false;
        } else {
            model.addAttribute("user", userFromDb);
            httpSession.setAttribute("user", userFromDb);
        }
        return true;
    }

    @Transactional
    public boolean saveUser(User user, Model model) {
        if (userDao.saveUser(user)) {
            model.addAttribute("msg", "Successfully registered . Now login!");
            return true;
        }
        model.addAttribute("msg", "Username is already taken!");
        return false;
    }
}
