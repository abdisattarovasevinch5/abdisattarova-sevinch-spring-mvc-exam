package uz.sevinch.controller;
//Sevinch Abdisattorova 02/25/2022 10:28 AM


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import uz.sevinch.model.Book;
import uz.sevinch.model.User;
import uz.sevinch.service.BookService;

import javax.servlet.http.HttpSession;

@Controller()
@RequestMapping("/books")
public class BookController {

    @Autowired
    BookService bookService;

    @GetMapping
    public String showBooks(Model model, HttpSession httpSession) {
        bookService.getAllBooks(model, httpSession);
        return "view-books";
    }

    @GetMapping("/form")
    public String getBookFormModel(Model model, @RequestParam(name = "id", required = false, defaultValue = "0") int id) {
        if (id != 0) {
            Book book = bookService.getBookById(id);
            model.addAttribute("book", book);
        }
        return "book-form";
    }


    @PostMapping
    public String saveBook(Book book, HttpSession session, Model model) {
        bookService.saveBook(book);
        User user = (User) session.getAttribute("user");
        model.addAttribute("user", user);
        return "redirect:/books";
    }

    @GetMapping("/delete/{id}")
    public String getBookFormModel(Model model, HttpSession session, @PathVariable(name = "id") Integer id) {
        bookService.deleteBookById(id);
        User user = (User) session.getAttribute("user");
        model.addAttribute("user", user);
        return "redirect:/books";
    }


}
