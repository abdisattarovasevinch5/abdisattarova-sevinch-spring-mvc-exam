package uz.sevinch.controller;
//Sevinch Abdisattorova 02/25/2022 9:06 AM


import com.sun.org.apache.xpath.internal.operations.Mod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import uz.sevinch.model.User;
import uz.sevinch.service.UserService;

import javax.servlet.http.HttpSession;

@Controller
public class AuthController {

    @Autowired
    UserService userService;


    @RequestMapping(path = "/login/form")
    public String showLoginForm() {
        return "login";
    }


    @RequestMapping(path = "/register/form")
    public String showRegisterForm() {
        return "register";
    }

    @RequestMapping(path = "/login")
    public String login(User user, Model model, HttpSession httpSession) {
        boolean result = userService.getUserByUsernameAndPassword(user, model, httpSession);
        if (result) {
            return "redirect:/books";
        } else {
            return "login";
        }
    }


    @RequestMapping(path = "/logout")
    public String logout(HttpSession session) {
        session.removeAttribute("user");
        return "redirect:/login";
    }
}
