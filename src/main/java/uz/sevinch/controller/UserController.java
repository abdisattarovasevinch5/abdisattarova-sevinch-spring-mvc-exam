package uz.sevinch.controller;
//Sevinch Abdisattorova 02/25/2022 9:06 AM

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import uz.sevinch.model.User;
import uz.sevinch.service.UserService;

@Controller
public class UserController {

    @Autowired
    UserService userService;


    @RequestMapping(path = "/users", method = RequestMethod.POST)
    public String saveUser(@ModelAttribute("user") User user,
                           Model model) {
        if (userService.saveUser(user, model)) {
            return "login";
        } else return "register";
    }
}
